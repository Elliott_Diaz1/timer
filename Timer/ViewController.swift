//
//  ViewController.swift
//  Timer
//
//  Created by Elliott Diaz on 5/7/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var timerLabel: UILabel!
    
    var timer    = NSTimer()
    var time     = 0
    var isPaused = false

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startAndStopButton(sender: AnyObject)
    {
        if isPaused
        {
            timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(ViewController.increaseTimer), userInfo: nil, repeats: true)
            isPaused = false
        } else
        {
            timer.invalidate()
            isPaused = true
        }
        
    }

    @IBAction func resetButton(sender: AnyObject)
    {
        time = 0
        timerLabel.text = "0"
    }
    
    func increaseTimer()
    {
        time += 1
        timerLabel.text = "\(time)"
        
    }


}

